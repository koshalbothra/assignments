using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDB.API.Repositories;
using IMDB.API.Repositories.Interfaces;
using IMDB.API.Services;
using IMDB.API.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace IMDB.API
{
	public class Startup
	{
		readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.Configure<ConnectionString>(Configuration.GetSection("ConnectionString"));
			services.AddControllers();
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "IMDB.API", Version = "v1" });
			});
			services.AddCors(options =>
			{
				options.AddPolicy(MyAllowSpecificOrigins,
								  builder =>
								  {
									  builder.WithOrigins("http://locahost:5500", "http://127.0.0.1:5500").AllowAnyHeader().AllowAnyMethod();
								  });
			});
			services.AddScoped<IActorRepository, ActorRepository>();
			services.AddScoped<IProducerRepository, ProducerRepository>();
			services.AddScoped<IGenreRepository, GenreRepository>();
			services.AddScoped<IMovieRepository, MovieRepository>();
			services.AddScoped<IActorService, ActorService>();
			services.AddScoped<IProducerService, ProducerService>();
			services.AddScoped<IGenreService, GenreService>();
			services.AddScoped<IMovieService, MovieService>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseSwagger();
				app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "IMDB.API v1"));

			}
			app.UseCors(MyAllowSpecificOrigins);

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
