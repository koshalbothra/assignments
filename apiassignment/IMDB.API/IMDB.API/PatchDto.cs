﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.API
{
	public class PatchDto
	{
		public string PropertyName { get; set; }
		public string PropertyValue { get; set; }
	}
}


