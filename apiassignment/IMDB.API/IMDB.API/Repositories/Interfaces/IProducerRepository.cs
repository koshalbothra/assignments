﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IMDB.API.Models.DB;

namespace IMDB.API.Repositories.Interfaces
{
	public interface IProducerRepository
	{
		public int Add(Producer producer);
		public IEnumerable<Producer> GetAll();
		public Producer GetById(int id);
		public void Update(Producer producer);
		public void Remove(int id);

		public void PartialUpdate(string sql, DynamicParameters dbArgs);

	}
}
