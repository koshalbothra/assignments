﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IMDB.API.Models.DB;

namespace IMDB.API.Repositories.Interfaces
{
	public interface IActorRepository
	{
		public int Add(Actor actor);
		public IEnumerable<Actor> GetAll();
		public Actor GetById(int id);
		public void Update(Actor actor);
		public void Remove(int id);
		public IEnumerable<Actor> GetByMovieId(int id);

		public void PartialUpdate(string sql, DynamicParameters dbArgs);

	}
}
