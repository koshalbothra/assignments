﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IMDB.API.Models.DB;
using IMDB.API.Models.Requests;
using IMDB.API.Models.Responses;
using IMDB.API.Repositories.Interfaces;
using IMDB.API.Services.Interfaces;
using Newtonsoft.Json.Linq;

namespace IMDB.API.Services
{
	public class MovieService : IMovieService
	{
		private readonly IMovieRepository _movieRepository;
		private readonly IProducerService _producerService;
		private readonly IActorService _actorService;
		private readonly IGenreService _genreService;

		public MovieService(IMovieRepository movieRepository,IProducerService producerService,IActorService actorService,IGenreService genreService)
		{
			_movieRepository = movieRepository;
			_producerService = producerService;
			_actorService = actorService;
			_genreService = genreService;
		}

		public void Validate(MovieRequest movie)
		{
			if (string.IsNullOrEmpty(movie.Name))
				throw new ArgumentNullException("Movie name can't be null or empty");
			if (string.IsNullOrEmpty(movie.Plot))
				throw new ArgumentNullException("Movie plot can't be null or empty");
			if (string.IsNullOrEmpty(movie.PosterURL))
				throw new ArgumentNullException("Movie poster url can't be null or empty");
			if (movie.YearOfRelease == 0)
				throw new ArgumentException("Movie year of release should not be zero");
		}

		public int Add(MovieRequest movieRequest)
		{
			Validate(movieRequest);
			var actorIds = string.Join(",",movieRequest.ActorIds);
			var genreIds = string.Join(",",movieRequest.GenreIds);
			return _movieRepository.Add(new Movie
			{
				Name = movieRequest.Name,
				YearOfRelease = movieRequest.YearOfRelease,
				Plot = movieRequest.Plot,
				PosterURL = movieRequest.PosterURL,
				ProducerId = movieRequest.ProducerId
			},actorIds,genreIds);
		}

		public IEnumerable<MovieResponse> GetAll()
		{
			return _movieRepository.GetAll().Select(movie => new MovieResponse
			{
				Id = movie.Id,
				Name = movie.Name,
				YearOfRelease = movie.YearOfRelease,
				Plot = movie.Plot,
				PosterURL = movie.PosterURL,
				Producer = _producerService.GetById(movie.ProducerId),
				Actors = _actorService.GetByMovieId(movie.Id).ToList(),
				Genres = _genreService.GetByMovieId(movie.Id).ToList()
			});
		}

		public MovieResponse GetById(int id)
		{
			var movie = _movieRepository.GetById(id);
			return new MovieResponse
			{
				Id = movie.Id,
				Name = movie.Name,
				YearOfRelease = movie.YearOfRelease,
				Plot = movie.Plot,
				PosterURL = movie.PosterURL,
				Producer = _producerService.GetById(movie.ProducerId),
				Actors = _actorService.GetByMovieId(movie.Id).ToList(),
				Genres = _genreService.GetByMovieId(movie.Id).ToList()
			};
		}

		public MovieResponse GetByName(string name)
		{
			var movie = _movieRepository.GetByName(name);
			return new MovieResponse
			{
				Id = movie.Id,
				Name = movie.Name,
				YearOfRelease = movie.YearOfRelease,
				Plot = movie.Plot,
				PosterURL = movie.PosterURL,
				Producer = _producerService.GetById(movie.ProducerId),
				Actors = _actorService.GetByMovieId(movie.Id).ToList(),
				Genres = _genreService.GetByMovieId(movie.Id).ToList()
			};
		}

		public void Remove(int id)
		{
			_movieRepository.GetById(id);
			_movieRepository.Remove(id);
		}



		public void Update(int id, MovieRequest movieRequest)
		{
			Validate(movieRequest);
			var actorIds = string.Join(",",movieRequest.ActorIds);
			var genreIds = string.Join(",",movieRequest.GenreIds);
			_movieRepository.Update(new Movie
			{
				Id = id,
				Name = movieRequest.Name,
				YearOfRelease = movieRequest.YearOfRelease,
				Plot = movieRequest.Plot,
				PosterURL = movieRequest.PosterURL,
				ProducerId = movieRequest.ProducerId
			}, actorIds, genreIds);
		}

		public void PartialUpdate(int id, List<PatchDto> patchDtos)
		{
			_movieRepository.GetById(id);
			var sql = @"Update Movies SET ";
			var sql2 = "";
			var sql3 = "";
			var dbArgs = new DynamicParameters();
			var properties = new List<string>();
			bool isEntered = false;
			foreach (var patchDto in patchDtos)
			{
				if (string.IsNullOrEmpty(patchDto.PropertyName))
					throw new ArgumentException("Property name can't be null or empty");
				if (string.IsNullOrEmpty(patchDto.PropertyValue))
					throw new ArgumentException("Property value can't be null or empty");
				switch (patchDto.PropertyName)
				{
					case "Name":
						properties.Add("Name=@Name");
						dbArgs.Add("@Name", patchDto.PropertyValue);
						isEntered = true;
						break;
					case "YearOfRelease":
						properties.Add("YearOfRelease=@YearOfRelease");
						try
						{
							dbArgs.Add("@YearOfRelease", int.Parse(patchDto.PropertyValue));
						}
						catch (Exception)
						{
							throw new Exception("YearOfRelease must be an integer");
						}
						isEntered = true;
						break;
					case "Plot":
						properties.Add("Plot=@Plot");
						dbArgs.Add("@Plot", patchDto.PropertyValue);
						isEntered = true;
						break;
					case "PosterURL":
						properties.Add("Poster=@PosterURL");
						dbArgs.Add("@PosterURL", patchDto.PropertyValue);
						isEntered = true;
						break;
					case "ProducerId":
						properties.Add("ProducerId=@ProducerId");
						try
						{
							dbArgs.Add("@ProducerId", int.Parse(patchDto.PropertyValue));
						}
						catch (Exception)
						{
							throw new Exception("ProducerId must be an integer");
						}
						isEntered = true;
						break;
					case "ActorIds":
						sql2 = "EXEC [usp_UpdateMovieActors] @Id, @ActorIds;";
						dbArgs.Add("@ActorIds", patchDto.PropertyValue);
						break;
					case "GenreIds":
						sql3 = "EXEC [usp_UpdateMovieGenres] @Id, @GenreIds;";
						dbArgs.Add("@GenreIds", patchDto.PropertyValue);
						break;
					default:
						throw new Exception($"Invalid property name '{patchDto.PropertyName}'");
				}
			}
			dbArgs.Add("@Id", id);
			if (isEntered == false)
				sql = sql2 + sql3;
			else
				sql += string.Join(',', properties) + " WHERE Id=@Id;" + sql2 + sql3;
			_movieRepository.PartialUpdate(sql, dbArgs);
		}

		//public void Patch(int id, string patchReq)
		//{
		//	GetById(id);
		//	//   var patch = JObject.Parse(patchReq);
		//	var patch = JArray.Parse(patchReq);
		//	//   var dbArgs = new List<DynamicParameters>();
		//	var dbArg = new DynamicParameters();
		//	List<string> fields = new List<string>();
		//	string sql = "";
		//	string sql2 = "";
		//	string sql3 = "";
		//	foreach (var p in patch)
		//	{
		//		var key = p["name"].ToString().ToUpper();
		//		var value = p["value"].ToString();
		//		switch (key)
		//		{
		//			case "PLOT":
		//				dbArg.Add("@Plot ", value);
		//				fields.Add("Plot = @Plot");
		//				break;
		//			case "YEAROFRELEASE":
		//				dbArg.Add("@YearOfRelease ", value);
		//				fields.Add("YearOfRelease = @YearOfRelease");
		//				break;
		//			case "NAME":
		//				dbArg.Add("@Name ", value);
		//				fields.Add("Name = @Name");
		//				break;
		//			case "POSTERURL":
		//				dbArg.Add("@PosterURL", value);
		//				fields.Add("Poster = @PosterURL");
		//				break;
		//			case "PRODUCERID":
		//				dbArg.Add("@ProducerId ", value);
		//				fields.Add("ProducerId = @ProducerId");
		//				break;
		//			case "ACTORIDS":
		//				dbArg.Add("@ActorIds", value);
		//				sql2 = "EXEC usp_UpdateMovieActors @movieId,@ActorIds;";
		//				break;
		//			case "GENREIDS":
		//				dbArg.Add("@GenreIds ", value);
		//				sql3 = "EXEC usp_UpdateMovieGenres @movieId,@GenreIds;";
		//				break;
		//			default:
		//				throw new Exception("Invalid Input");
		//		}
		//		//   dbArgs.Add(dbArg);
		//	}
		//	// var dbAr = new DynamicParameters();
		//	dbArg.Add("@movieId", id);
		//	// dbArgs.Add(dbAr);
		//	if(fields.Count == 0)
		//	sql = sql2 + sql3;
		//	else
		//	sql = $"update movies set {string.Join(",", fields) } where id = @movieId;"+sql2+sql3;
		//	_movieRepository.Patch(sql, dbArg);

		//	//"[{\"name\":\"ActorIds\",\"value\":\"9,10\"},{\"name\":\"GenreIds\",\"value\":\"4,5\"}]"

		//}

	}
}
