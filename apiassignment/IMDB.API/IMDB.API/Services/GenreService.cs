﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IMDB.API.Models.DB;
using IMDB.API.Models.Requests;
using IMDB.API.Models.Responses;
using IMDB.API.Repositories.Interfaces;
using IMDB.API.Services.Interfaces;

namespace IMDB.API.Services
{
	public class GenreService : IGenreService
	{
		private readonly IGenreRepository _genreRepository;

		public GenreService(IGenreRepository genreRepository)
		{
			_genreRepository = genreRepository;
		}

		public void Validate(GenreRequest genre)
		{
			if (string.IsNullOrEmpty(genre.Name))
				throw new ArgumentNullException("Genre name can't be null or empty");
		}
		public int Add(GenreRequest genreRequest)
		{
			Validate(genreRequest);
			return _genreRepository.Add(new Genre
			{
				Name = genreRequest.Name
			});
		}

		public IEnumerable<GenreResponse> GetAll()
		{
			return _genreRepository.GetAll().Select(genre => new GenreResponse
			{
				Id = genre.Id,
				Name = genre.Name
			});
		}

		public GenreResponse GetById(int id)
		{
			var genre = _genreRepository.GetById(id);
			return new GenreResponse
			{
				Id = genre.Id,
				Name = genre.Name
			};
		}

		public IEnumerable<GenreResponse> GetByMovieId(int id)
		{
			return _genreRepository.GetByMovieId(id).Select(genre => new GenreResponse
			{
				Id = genre.Id,
				Name = genre.Name
			});
		}

		public void Remove(int id)
		{
			_genreRepository.GetById(id);
			_genreRepository.Remove(id);
		}

		public void PartialUpdate(int id, List<PatchDto> patchDtos)
		{
			_genreRepository.GetById(id);
			var sql = @"Update Genres SET ";
			var dbArgs = new DynamicParameters();
			var properties = new List<string>();
			foreach (var patchDto in patchDtos)
			{
				if (string.IsNullOrEmpty(patchDto.PropertyName))
					throw new ArgumentException("Property name can't be null or empty");
				if (string.IsNullOrEmpty(patchDto.PropertyValue))
					throw new ArgumentException("Property value can't be null or empty");
				if (!patchDto.PropertyName.Equals("Name"))
					throw new Exception($"Invalid property name '{patchDto.PropertyName}'");
				properties.Add("Name=@Name");
				dbArgs.Add("@Name", patchDto.PropertyValue);
			}
			dbArgs.Add("@Id", id);
			sql += string.Join(',', properties) + " WHERE Id=@Id";
			_genreRepository.PartialUpdate(sql, dbArgs);
		}
		public void Update(int id, GenreRequest genreRequest)
		{
			Validate(genreRequest);
			_genreRepository.GetById(id);
			_genreRepository.Update(new Genre
			{
				Id = id,
				Name = genreRequest.Name
			});
		}
	}
}
