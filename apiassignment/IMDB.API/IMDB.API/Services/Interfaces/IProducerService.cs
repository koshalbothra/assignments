﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDB.API.Models.Requests;
using IMDB.API.Models.Responses;

namespace IMDB.API.Services.Interfaces
{
	public interface IProducerService
	{
		public int Add(ProducerRequest producerRequest);
		public IEnumerable<ProducerResponse> GetAll();
		public ProducerResponse GetById(int id);
		public void Update(int id, ProducerRequest producerRequest);
		public void Remove(int id);
		public void PartialUpdate(int id, List<PatchDto> patchDtos);
	}
}
