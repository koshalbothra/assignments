﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDB.API.Models.Requests;
using IMDB.API.Models.Responses;

namespace IMDB.API.Services.Interfaces
{
	public interface IGenreService
	{
		public int Add(GenreRequest genreRequest);
		public IEnumerable<GenreResponse> GetAll();
		public GenreResponse GetById(int id);
		public void Update(int id, GenreRequest genreRequest);
		public void Remove(int id);
		public IEnumerable<GenreResponse> GetByMovieId(int id);
		public void PartialUpdate(int id, List<PatchDto> patchDtos);
	}
}
