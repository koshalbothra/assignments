﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDB.API.Models.Requests;
using IMDB.API.Models.Responses;

namespace IMDB.API.Services.Interfaces
{
	public interface IMovieService
	{
		public int Add(MovieRequest movieRequest);
		public IEnumerable<MovieResponse> GetAll();
		public MovieResponse GetById(int id);
		public MovieResponse GetByName(string name);
		public void Update(int id, MovieRequest movieRequest);
		public void Remove(int id);
		//public void Patch(int id, string movie);
		public void PartialUpdate(int id, List<PatchDto> patchDtos);
	}

}
