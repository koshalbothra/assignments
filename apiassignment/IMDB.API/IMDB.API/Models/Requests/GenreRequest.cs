﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.API.Models.Requests
{
    public class GenreRequest
    {
        public string Name { get; set; }

    }
}
