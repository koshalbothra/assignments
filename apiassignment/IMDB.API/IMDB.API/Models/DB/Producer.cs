﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB.API.Models.DB
{
	public class Producer
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Bio { get; set; }
		public DateTime DOB { get; set; }
		public string Sex { get; set; }
	}
}
