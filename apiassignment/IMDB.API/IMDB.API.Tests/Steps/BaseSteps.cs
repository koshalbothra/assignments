﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using TechTalk.SpecFlow;
using Xunit;

namespace IMDB.API.Tests.Steps
{
    [Binding]
    public class BaseSteps
    {
        protected HttpClient Client { get; set; }
        protected WebApplicationFactory<TestStartup> Factory { get; set; }

        protected HttpResponseMessage Response { get; set; }

        public BaseSteps(WebApplicationFactory<TestStartup> factory)
        {
            Factory = factory;
        }
        [Given(@"I am a client")]
        public void GivenIAmAClient()
        {
            Client = Factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                BaseAddress = new Uri("http://localhost")
            });
        }

        [When(@"I make GET Request '(.*)'")]
        public async Task WhenIMakeGETRequest(string endPoint)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            Response = await Client.GetAsync(uri).ConfigureAwait(false);
        }

        [Then(@"response code must be '(.*)'")]
        public void ThenResponseCodeMustBe(int statusCode)
        {
            var expectedStatusCode = (HttpStatusCode)statusCode;
            Assert.Equal(expectedStatusCode, Response.StatusCode);
        }

        [Then(@"response data must look like '(.*)'")]
        public async Task ThenResponseDataMustLookLike(string response)
        {
            var responseData = await Response.Content.ReadAsStringAsync().ConfigureAwait(false);
            Assert.Equal(response, responseData);
        }

        [When(@"I make POST request '(.*)' with following data '(.*)'")]
        public async Task WhenIMakePOSTRequestWithFollowingData(string endPoint, string postData)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            var content = new StringContent(postData, Encoding.UTF8, "application/json");
            Response = await Client.PostAsync(uri, content).ConfigureAwait(false);
        }

        [When(@"I make DELETE request '(.*)'")]
        public async Task WhenIMakeDELETERequest(string endPoint)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            Response = await Client.DeleteAsync(uri).ConfigureAwait(false);
        }

        [When(@"I make PUT request '(.*)' with following data '(.*)'")]
        public async Task WhenIMakePUTRequestWithFollowingData(string endPoint, string putData)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            var content = new StringContent(putData, Encoding.UTF8, "application/json");
            Response = await Client.PutAsync(uri, content).ConfigureAwait(false);
        }

        [When(@"I make PATCH request '(.*)' with following data '(.*)'")]
        public async Task WhenIMakePATCHRequestWithFollowingData(string endPoint, string postData)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            var content = new StringContent(postData, Encoding.UTF8, "application/json");
            Response = await Client.PatchAsync(uri, content).ConfigureAwait(false);
        }


    }
}
