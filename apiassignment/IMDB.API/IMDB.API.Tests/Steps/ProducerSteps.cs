﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using IMDB.API.Tests.MockRepositories;
using TechTalk.SpecFlow;

namespace IMDB.API.Tests.Steps
{
	[Scope(Feature = "Producer Resource")]
	[Binding]
	public class ProducerSteps : BaseSteps
	{
		public ProducerSteps(CustomWebApplicationFactory<TestStartup> factory)
			: base(factory.WithWebHostBuilder(builder => {
				builder.ConfigureServices(services =>
				{
					services.AddScoped(services => ProducerMock.producerRepoMock.Object);
				});
			}))
		{

		}

		[BeforeScenario]
		public void MockRepositories()
		{
			ProducerMock.MockGetAll();
			ProducerMock.MockGetById();
			ProducerMock.MockAdd();
			ProducerMock.MockUpdate();
			ProducerMock.MockRemove();
			ProducerMock.MockPartialUpdate();
		}
	}
}
