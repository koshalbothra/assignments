﻿Feature: Actor Resource
Background: 
    Given I am a client

@GetAllActors
Scenario: Get all Actors
    When I make GET Request '<route>'
    Then response code must be '<status code>'
    And response data must look like '<response>'
    Examples: 
    | route   | status code | response                                                                                                                                                                             |
    | /actors | 200         | [{"id":1,"name":"Christian Bale","bio":"British","dob":"1979-03-02T00:00:00","sex":"Male"},{"id":2,"name":"Mila Kunis","bio":"Ukranian","dob":"1973-06-22T00:00:00","sex":"Female"}] |

@GetActorById
Scenario: Get Actor by Id
    When I make GET Request '<route>'
    Then response code must be '<status code>'
    And response data must look like '<response>'
    Examples: 
    | route      | status code | response                                                                                 |
    | /actors/2  | 200         | {"id":2,"name":"Mila Kunis","bio":"Ukranian","dob":"1973-06-22T00:00:00","sex":"Female"} |
    | /actors/10 | 404         | No Actor is present with given id                                                        |

@AddActor
Scenario: Add Actor
    When I make POST request '<route>' with following data '<request>'
    Then response code must be '<status code>'
    And response data must look like '<response>'
    Examples: 
    | route   | request                                                                    | status code | response                                                              |
    | /actors | {"name":"Christian Bale","bio":"British","dob":"1979-03-02","sex":"Male"}  | 200         | {"id":1}                                                              |
    | /actors | {"name":"","bio":"British","dob":"1979-03-02","sex":"Male"}                | 400         | Value cannot be null. (Parameter 'Actor name can't be null or empty') |
    | /actors | {"name":"Christian Bale","bio":"","dob":"1979-03-02","sex":"Male"}         | 400         | Value cannot be null. (Parameter 'Actor bio can't be null or empty')  |
    | /actors | {"name":"Christian Bale","bio":"British","dob":"2021-05-02","sex":"Male"}  | 400         | Actor date of birth should be before current date                     |
    | /actors | {"name":"Christian Bale","bio":"British","dob":"1979-03-02","sex":""}      | 400         | Value cannot be null. (Parameter 'Actor sex can't be null or empty')  |
    | /actors | {"name":"Christian Bale","bio":"British","dob":"1979-03-02","sex":"Hello"} | 400         | Actor sex is invalid                                                  |

@RemoveActor
Scenario: Remove Actor
    When I make DELETE request '<route>'
    Then response code must be '<status code>'
    And response data must look like '<response>'
    Examples: 
    | route       | status code | response                          |
    | /actors/1   | 200         | {"id":1}                          |
    | /actors/10  | 404         | No Actor is present with given id |

@UpdateActor
Scenario: Update Actor
    When I make PUT request '<route>' with following data '<request>'
    Then response code must be '<status code>'
    And response data must look like '<response>'
    Examples: 
    | route      | request                                                                    | status code | response                                                              |
    | /actors/1  | {"name":"Christian Bale","bio":"American","dob":"1979-03-02","sex":"Male"} | 200         | {"id":1}                                                              |
    | /actors/10 | {"name":"Christian Bale","bio":"American","dob":"1979-03-02","sex":"Male"} | 404         | No Actor is present with given id                                     |
    | /actors/1  | {"name":"","bio":"British","dob":"1979-03-02","sex":"Male"}                | 400         | Value cannot be null. (Parameter 'Actor name can't be null or empty') |
    | /actors/1  | {"name":"Christian Bale","bio":"","dob":"1979-03-02","sex":"Male"}         | 400         | Value cannot be null. (Parameter 'Actor bio can't be null or empty')  |
    | /actors/1  | {"name":"Christian Bale","bio":"British","dob":"2021-05-02","sex":"Male"}  | 400         | Actor date of birth should be before current date                     |
    | /actors/1  | {"name":"Christian Bale","bio":"British","dob":"1979-03-02","sex":""}      | 400         | Value cannot be null. (Parameter 'Actor sex can't be null or empty')  |
    | /actors/1  | {"name":"Christian Bale","bio":"British","dob":"1979-03-02","sex":"Hello"} | 400         | Actor sex is invalid                                                  |


    
@PartialUpdateActor
Scenario: Partial Update Actor
    When I make PATCH request '<route>' with following data '<request>'
    Then response code must be '<status code>'
    And response data must look like '<response>'
    Examples: 
    | route      | request                                                                                            | status code | response                                          |
    | /actors/2  | [{"propertyName":"Name","propertyValue":"Mila"},{"propertyName":"Bio","propertyValue":"American"}] | 200         | {"id":2}                                          |
    | /actors/2  | [{"propertyName":"","propertyValue":"Mila"}]                                                       | 400         | Property name can't be null or empty              |
    | /actors/2  | [{"propertyName":"Name","propertyValue":""}]                                                       | 400         | Property value can't be null or empty             |
    | /actors/2  | [{"propertyName":"DOB","propertyValue":"2021-10-28"}]                                              | 400         | Actor date of birth should be before current date |
    | /actors/2  | [{"propertyName":"DOB","propertyValue":"1999-10-"}]                                                | 400         | Actor date of birth should be a valid date        |
    | /actors/2  | [{"propertyName":"Sex","propertyValue":"Hello"}]                                                   | 400         | Actor sex is invalid                              |
    | /actors/2  | [{"propertyName":"Hello","propertyValue":"Mila"}]                                                  | 400         | Invalid property name 'Hello'                     |
    | /actors/10 | [{"propertyName":"Name","propertyValue":"Mila"}]                                                   | 404         | No Actor is present with given id '10'            |
    | /actors/2  | [{"propertyName":"Name","propertyValue":"Mila"},{"propertyName":"Name","propertyValue":"Kunis"}]   | 400         | A property is given more than once                |