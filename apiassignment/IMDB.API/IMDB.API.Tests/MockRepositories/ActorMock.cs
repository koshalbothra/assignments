﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Dapper;
using IMDB.API.Models.DB;
using IMDB.API.Repositories.Interfaces;
using Moq;

namespace IMDB.API.Tests.MockRepositories
{
	public class ActorMock
	{
		public static readonly Mock<IActorRepository> actorRepoMock = new Mock<IActorRepository>();

		public static void MockGetAll()
		{
			actorRepoMock.Setup(repo => repo.GetAll()).Returns(() =>
			{
				return ListOfActors();
			});
		}

		public static void MockGetById()
		{
			actorRepoMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((int id) =>
			{
				if (id == 10)
					throw new Exception("Sequence contains no elements");
				return ListOfActors().First(actor => actor.Id == id);
			});
		}

		public static void MockGetByMovieId()
		{
			actorRepoMock.Setup(repo => repo.GetByMovieId(It.IsAny<int>())).Returns((int movieId) =>
			  new List<Actor>
			  {
				  new Actor
				  {
					  Id=1,
					  Name="Christian Bale",
					  Sex="Male",
					  DOB=new DateTime(1979,03,02),
					  Bio="British"
				  },
				  new Actor
				  {
					  Id=2,
					  Name="Mila Kunis",
					  Sex="Female",
					  DOB=new DateTime(1973,06,22),
					  Bio="Ukranian"
				  }
			  });
		}

		public static void MockAdd()
		{
			actorRepoMock.Setup(repo => repo.Add(It.IsAny<Actor>())).Returns(1);
		}

		public static void MockRemove()
		{
			actorRepoMock.Setup(repo => repo.Remove(It.IsAny<int>()));
		}

		public static void MockUpdate()
		{
			actorRepoMock.Setup(repo => repo.Update(It.IsAny<Actor>()));
		}

		public static void MockPartialUpdate()
		{
			actorRepoMock.Setup(repo => repo.PartialUpdate(It.IsAny<string>(), It.IsAny<DynamicParameters>())).Callback((string sql, DynamicParameters dbArgs) =>
			{
				int count = new Regex("@Name", RegexOptions.Compiled | RegexOptions.IgnoreCase).Matches(sql).Count;
				if (count > 1)
					throw new Exception("Property is specified more than once");
			});
		}
		private static IEnumerable<Actor> ListOfActors()
		{
			var list = new List<Actor>
			{
				new Actor
				{
					Id=1,
					Name="Christian Bale",
					Sex="Male",
					DOB=new DateTime(1979,03,02),
					Bio="British"
				},
				new Actor
				{
					Id=2,
					Name="Mila Kunis",
					Sex="Female",
					DOB=new DateTime(1973,06,22),
					Bio="Ukranian"
				}
			};
			return list;
		}
	}
}
