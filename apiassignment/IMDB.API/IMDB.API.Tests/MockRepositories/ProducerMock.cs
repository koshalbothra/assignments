﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Dapper;
using IMDB.API.Models.DB;
using IMDB.API.Repositories.Interfaces;
using Moq;

namespace IMDB.API.Tests.MockRepositories
{
	public class ProducerMock
	{
		public static readonly Mock<IProducerRepository> producerRepoMock = new Mock<IProducerRepository>();

		public static void MockGetAll()
		{
			producerRepoMock.Setup(repo => repo.GetAll()).Returns(ListOfProducers());
		}

		public static void MockGetById()
		{
			producerRepoMock.Setup(repo => repo.GetById(It.IsAny<int>()))
				.Returns((int id) =>
				{
					if (id == 10)
						throw new Exception("Sequence contains no elements");
					return ListOfProducers().First(producer => producer.Id == id);
				});
		}

		public static void MockAdd()
		{
			producerRepoMock.Setup(repo => repo.Add(It.IsAny<Producer>())).Returns(1);
		}

		public static void MockUpdate()
		{
			producerRepoMock.Setup(repo => repo.Update(It.IsAny<Producer>()));
		}
		public static void MockPartialUpdate()
		{
			producerRepoMock.Setup(repo => repo.PartialUpdate(It.IsAny<string>(), It.IsAny<DynamicParameters>())).Callback((string sql, DynamicParameters dbArgs) =>
			{
				int count = new Regex("@Name", RegexOptions.Compiled | RegexOptions.IgnoreCase).Matches(sql).Count;
				if (count > 1)
					throw new Exception("Property is specified more than once");
			});
		}
		public static void MockRemove()
		{
			producerRepoMock.Setup(repo => repo.Remove(It.IsAny<int>()));
		}
		private static IEnumerable<Producer> ListOfProducers()
		{
			var list = new List<Producer>
			{
				new Producer
				{
					Id=1,
					Name="Zeshan",
					Bio="American",
					DOB=new DateTime(1979,03,02),
					Sex="Male"
				},
				new Producer
				{
					Id=2,
					Name="Nicole",
					Bio="British",
					DOB=new DateTime(1975,09,02),
					Sex="Male"
				}
			};
			return list;
		}
	}
}