Create Database SQLAssignment2
USE SQLAssignment2

CREATE TABLE Actors( 
Id INT PRIMARY KEY IDENTITY(1,1), 
Name NVARCHAR(30), 
Gender NVARCHAR(10),
DOB DATE,
CreatedAt  DATETIME DEFAULT CURRENT_TIMESTAMP)

CREATE TABLE Producers( 
Id INT PRIMARY KEY IDENTITY(1,1),
Name NVARCHAR(30),
Company NVARCHAR(30),
CompanyEstDate DATE,
CreatedAt  DATETIME DEFAULT CURRENT_TIMESTAMP)

CREATE TABLE Movies(Id INT PRIMARY KEY IDENTITY(1,1),
Name NVARCHAR(30),
Language NVARCHAR(10),
ProducerId INT FOREIGN KEY REFERENCES Producers(Id),
Profit INT,
CreatedAt  DATETIME DEFAULT CURRENT_TIMESTAMP)

CREATE TABLE ActorMovieMapping( Id INT PRIMARY KEY IDENTITY(1,1),
MovieId INT FOREIGN KEY REFERENCES Movies(Id),
ActorId INT FOREIGN KEY REFERENCES Actors(Id),
CreatedAt  DATETIME DEFAULT CURRENT_TIMESTAMP)

INSERT INTO Actors(Name, Gender, DOB) VALUES('Mila Kunis','Female','11/14/1986')
INSERT INTO Actors(Name, Gender, DOB) VALUES('Robert DeNiro','Male','07/10/1957')
INSERT INTO Actors(Name, Gender, DOB) VALUES('George Michael','Male','11/23/1978')
INSERT INTO Actors(Name, Gender, DOB) VALUES('Mike Scott','Male','08/06/1969')
INSERT INTO Actors(Name, Gender, DOB) VALUES('Pam Halpert','Female','09/26/1996')
INSERT INTO Actors(Name, Gender, DOB) VALUES('Dame Judi Dench','Female','04/05/1947')

SELECT * FROM Actors


INSERT INTO Producers(Name, Company,CompanyEstDate) VALUES('Arjun','Fox','05/14/1998')
INSERT INTO Producers(Name, Company,CompanyEstDate) VALUES('Arun','Bull','09/11/2004')
INSERT INTO Producers(Name, Company,CompanyEstDate) VALUES('Tom','Hanks','11/03/1987')
INSERT INTO Producers(Name, Company,CompanyEstDate) VALUES('Zeshan','Male','11/14/1996')
INSERT INTO Producers(Name, Company,CompanyEstDate) VALUES('Nicole','Team Coco','09/26/1992')

SELECT * FROM Producers

INSERT INTO Movies(Name, Language, ProducerId, Profit) VALUES('Rocky','English',1,10000)
INSERT INTO Movies(Name, Language, ProducerId, Profit) VALUES('Rocky','Hindi',3,3000)
INSERT INTO Movies(Name, Language, ProducerId, Profit) VALUES('Terminal','English',4,300000)
INSERT INTO Movies(Name, Language, ProducerId, Profit) VALUES('Rambo','Hindi',2,93000)
INSERT INTO Movies(Name, Language, ProducerId, Profit) VALUES('Rudy','English',5,9600)

SELECT * FROM Movies


INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(1,1)
INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(1,3)
INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(1,5)

INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(2,6)
INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(2,5)
INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(2,4)
INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(2,2)

INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(3,3)
INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(3,2)

INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(4,1)
INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(4,6)
INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(4,3)

INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(5,2)
INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(5,5)
INSERT INTO ActorMovieMapping(MovieId,ActorId) VALUES(5,3)

SELECT * FROM ActorMovieMapping

--Update Profit of all the movies by +1000 where producer name contains 'run'

UPDATE Movies SET Profit = Profit + 1000 
FROM Movies M INNER JOIN Producers P ON M.ProducerId = P.Id 
WHERE P.Name LIKE '%run%'


--Find duplicate movies having the same name and their count

SELECT Name AS MovieName, COUNT(*) AS MovieCount 
FROM Movies 
GROUP    BY Name 
HAVING COUNT(*) > 1

--Find the oldest actor/actress for each movie

SELECT R.Name AS Movie_Name , A.Name AS Actor_Name from (SELECT M.Id,M.Name, MIN(A.DOB) AS MinimumDOB
FROM Actors A INNER JOIN ActorMovieMapping AM ON A.Id = AM.ActorId
INNER JOIN Movies M ON AM.MovieId = M.Id
GROUP BY M.Id,M.Name)R JOIN ActorMovieMapping AM ON AM.MovieId = R.Id JOIN Actors A ON A.Id = AM.ActorId
WHERE A.DOB = MinimumDOB

--List of producers who have not worked with actor X

SELECT name AS Producers_Name FROM Producers 
WHERE id NOT IN(SELECT P.id
FROM Producers P INNER JOIN Movies M on P.Id = M.ProducerId
INNER JOIN ActorMovieMapping AM on M.Id = AM.MovieId
INNER JOIN Actors A on AM.ActorId = A.Id
WHERE A.name = 'Mila Kunis')


--List of pair of actors who have worked together in more than 2 movies

SELECT a1.name AS Actor_Name1, a2.name AS Actor_Name2 FROM Actors a1
INNER JOIN 
(SELECT am1.ActorId,am2.ActorId as ActorId2, COUNT(*) cnt
FROM ActorMovieMapping am1 JOIN ActorMovieMapping am2 ON (am1.ActorId > am2.ActorId AND am1.MovieId = am2.MovieId)
GROUP BY am1.ActorId,am2.ActorId
HAVING COUNT(*) >= 2) r ON a1.Id = r.ActorId
INNER JOIN  Actors a2 ON r.ActorId2 = a2.Id


--Add non-clustered index on profit column of movies table

CREATE NONCLUSTERED INDEX Movies_Profit ON Movies(Profit) 

--Create stored procedure to return list of actors for given movie id

EXEC SelectAllActors @MovieId =1;


--Create a function to return age for given date of birth

SELECT dbo.AGE(A.DOB) as Actors_Age FROM Actors A



--Create a stored procedure to increase the profit (+100) of movies with given Ids (comma separated)

EXEC IncreaseProfit @MovieIds ='1,2,3'





--7. CREATE PROCEDURE SelectAllActorss @MovieID INT
--AS
--SELECT A.Name 
--FROM Actors A 
--INNER JOIN ActorMovieMapping AMM
--ON AMM.ActorId = A.Id
--WHERE AMM.MovieId = @MovieID
--GO
--EXEC SelectAllActorss @MovieId =1;



--8.CREATE FUNCTION AGE(@DateOfBirth AS DATETIME)

--RETURNS INT

--AS

--BEGIN

--DECLARE @Years AS INT

--DECLARE @BirthdayDate AS DATETIME

--DECLARE @Age AS INT

----Calculate difference in years

--SET @Years = DATEDIFF(YY,@DateOfBirth,GETDATE())

----Add years to DateOfBirth

--SET @BirthdayDate = DATEADD(YY,@Years,@DateOfBirth)

----Subtract a year if birthday is after today

--SET @Age = @Years -

--CASE

--WHEN @BirthdayDate>GETDATE() THEN 1

--ELSE 0

--END

----Return the result

--RETURN @Age

--END

--9.CREATE PROCEDURE IncreaseProfit @MovieIds varchar(20)
--AS
--UPDATE Movies SET Profit = Profit + 100 WHERE Id IN (SELECT * FROM string_split(@MovieIds,','))
--GO
