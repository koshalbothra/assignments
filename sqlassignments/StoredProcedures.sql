CREATE PROCEDURE usp_AddMovie @Name NVARCHAR(MAX)
,@YearOfRelease INT
,@ProducerId INT
,@Plot NVARCHAR(MAX)
,@PosterURL NVARCHAR(MAX)
,@ActorIds NVARCHAR(MAX)
,@GenreIds NVARCHAR(MAX)

AS

INSERT INTO Movies
(
Name , 
YearOfRelease , 
Plot ,
Poster,
ProducerId
)
VALUES(@Name
,@YearOfRelease
,@Plot
,@PosterURL
,@ProducerId
);


DECLARE @MovieId AS INT

SET @MovieId = SCOPE_IDENTITY();

INSERT INTO MovieActorMapping
(
MovieId,
ActorId
)
SELECT @MovieId [MovieId]
,[value] [ActorId]
FROM STRING_SPLIT(@ActorIds,',');

INSERT INTO MovieGenreMapping
(
MovieId,
GenreId
)
SELECT @MovieId [MovieId]
,[value] [GenreId]
FROM STRING_SPLIT(@GenreIds,',');

GO

CREATE PROCEDURE usp_UpdateMovie @MovieId INT
,@Name NVARCHAR(MAX)
,@YearOfRelease INT
,@ProducerId INT
,@Plot NVARCHAR(MAX)
,@PosterURL NVARCHAR(MAX)
,@ActorIds NVARCHAR(MAX)
,@GenreIds NVARCHAR(MAX)

AS

UPDATE Movies
SET Name=@Name
,YearOfRelease=@YearOfRelease
,ProducerId=@ProducerId
,Plot=@Plot
,Poster=@PosterURL
WHERE Id=@MovieId;

DECLARE @MId AS INT
SET @MId=@MovieId

DELETE
FROM MovieActorMapping
WHERE MovieId=@MovieId;

DELETE
FROM MovieGenreMapping
WHERE MovieId=@MovieId;

INSERT INTO MovieActorMapping
(
MovieId,
ActorId
)
SELECT @MId [MovieId]
,[value] [ActorId]
FROM STRING_SPLIT(@ActorIds,',');

INSERT INTO MovieGenreMapping
(
MovieId,
GenreId
)
SELECT @MId [MovieId]
,[value] [GenreId]
FROM STRING_SPLIT(@GenreIds,',');

GO

CREATE PROCEDURE usp_DeleteMovie @Id INT
AS

DELETE FROM MovieActorMapping
WHERE MovieId=@Id;

DELETE FROM MovieGenreMapping
WHERE MovieId=@Id;

DELETE FROM Movies
WHERE Id=@Id;

GO

CREATE PROCEDURE usp_DeleteActor @Id INT
AS

DELETE FROM MovieActorMapping
WHERE ActorId=@Id;

DELETE FROM Actors
WHERE Id=@Id;

GO

CREATE PROCEDURE usp_DeleteGenre @Id INT
AS

DELETE FROM MovieGenreMapping
WHERE GenreId=@Id;

DELETE FROM Genres
WHERE Id=@Id;

GO


CREATE PROCEDURE usp_UpdateMovieActors @MovieId INT,
@ActorIds NVARCHAR(MAX)
AS

DELETE
FROM MovieActorMapping
WHERE MovieId= @MovieId;

INSERT INTO MovieActorMapping
(
MovieId,
ActorId
)
SELECT @MovieId [MovieId]
,[value] [ActorId]
FROM STRING_SPLIT(@ActorIds,',');
GO

CREATE PROCEDURE usp_UpdateMovieGenres @MovieId INT,
@GenreIds NVARCHAR(MAX)
AS

DELETE
FROM MovieGenreMapping
WHERE MovieId= @MovieId;

INSERT INTO MovieGenreMapping
(
MovieId,
GenreId
)
SELECT @MovieId [MovieId]
,[value] [GenreId]
FROM STRING_SPLIT(@GenreIds,',');
GO


