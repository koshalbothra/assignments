Create Database SQLAssignment
USE SQLAssignment

Create TABLE Classes
(
Id INT Primary Key Identity(1,1),
Name Varchar(100),
Section Varchar(100),
Number INT
)

INSERT INTO Classes values ('IX','A',201)
INSERT INTO Classes values ('IX','B',202)
INSERT INTO Classes values ('X','A',203)

Create TABLE Teachers
(
Id INT Primary Key Identity(1,1),
Name Varchar(100),
DOB date,
Gender Varchar(20)
)

INSERT INTO Teachers values ('Lisa Kudrow','1985/06/08','Female')
INSERT INTO Teachers values ('Monica Bing','1982/03/06','Female')
INSERT INTO Teachers values ('Chandler Bing','1978/12/17','Male')
INSERT INTO Teachers values ('Ross Geller','1993/01/26','Male')



Create TABLE Students
(
Id INT Primary Key Identity(1,1),
Name Varchar(100),
DOB date,
Gender Varchar(20),
ClassId INT
)

INSERT INTO Students values ('Scotty Loman','2006/01/31','Male',1)
INSERT INTO Students values ('Adam Scott','2005/06/01','Male',1)
INSERT INTO Students values ('Natosha Beckles','2005/01/23','Female',2)
INSERT INTO Students values ('Lilly Page','2006/11/26','Female',2)
INSERT INTO Students values ('John Freeman','2006/06/14','Male',2)
INSERT INTO Students values ('Morgan Scott','2005/05/18','Male',3)
INSERT INTO Students values ('Codi Gass','2005/12/24','Female',3)
INSERT INTO Students values ('Nick Roll','2005/12/24','Male',3)
INSERT INTO Students values ('Dave Grohl','2005/02/12','Male',3)



Create TABLE TeacherClassMapping
(
TeacherID INT ,
ClassId INT 
)

INSERT INTO TeacherClassMapping values (1,1)
INSERT INTO TeacherClassMapping values (1,2)
INSERT INTO TeacherClassMapping values (2,2)
INSERT INTO TeacherClassMapping values (2,3)
INSERT INTO TeacherClassMapping values (3,3)
INSERT INTO TeacherClassMapping values (3,1)



--Find list of male students 
SELECT * FROM Students
WHERE Gender ='MALE'


--Find list of student older than 2005/01/01
SELECT * FROM Students
WHERE DOB > '2005/01/01'

--Youngest student in school
SELECT * FROM Students 
WHERE DOB =(SELECT MAX(DOB) FROM Students)

--Find student distinct birthdays
SELECT DISTINCT (DOB) from Students

-- No of students in each class
SELECT C.Name, COUNT(*) AS Student_Count
FROM Students S
JOIN Classes C
ON S.ClassId = C.Id
GROUP BY C.Name

-- No of students in each section
SELECT C.Section, COUNT(*) AS Student_Count
FROM Students S
JOIN Classes C
ON S.ClassId = C.Id
GROUP BY C.Section

-- No of classes taught by teacher
SELECT T.Id,T.Name,COUNT(*)as Count
FROM Teachers T
JOIN TeacherClassMapping TCM
on T.Id = TCM.TeacherID
GROUP BY T.Id,T.Name

-- List of teachers teaching Class X
SELECT T.name as Teachers_Name
FROM Teachers T
JOIN TeacherClassMapping TCM
ON T.ID = TCM.TeacherID 
JOIN Classes C
ON C.ID = TCM.ClassId 
AND C.Name ='X'

-- Classes which have more than 2 teacher teaching

SELECT C.name as Class_Name 
FROM Classes C
JOIN TeacherClassMapping TCM
ON TCM.ClassId = C.Id
JOIN Teachers T
ON T.id = TCM.TeacherId
GROUP BY C.name
HAVING COUNT(TCM.TeacherId)>2

--List of students being taught by 'Lisa'

SELECT  S.* 
FROM Students S 
JOIN TeacherClassMapping TCM 
ON TCM.ClassId = S.ClassId
JOIN Teachers T 
ON T.ID = TCM.TeacherId
WHERE T.name LIKE 'Lisa%'