$(document).ready(function () {
    $("#genresOption").select2();
    $('#actorsOption').select2();
    $.ajax({ url: "https://localhost:5001/producers", type: "GET", success: producerSuccess, error: producerError });
    $.ajax({ url: "https://localhost:5001/actors", type: "GET", success: actorSuccess, error: actorError });
    $.ajax({ url: "https://localhost:5001/genres", type: "GET", success: genreSuccess, error: genreError });
});
var producers = [];
var actors = [];
var genres = [];




function producerSuccess(response) {
    producers = response;
    let childs = [];
    producers.forEach(producer => {
        childs.push(`<option value="${producer.id}">${producer.name}</option>`);
    });
    $('#producerSelect').empty()
    $("#producerSelect").html(childs.join(''));
    console.log("success");
}

function producerError(error) {
    console.log(error);
}

function actorSuccess(response) {
    actors = response;
    let childs = [];
    actors.forEach(actor => {
        childs.push(`<option value="${actor.id}">${actor.name}</option>`);
    });
    $('#actorOptions').empty()
    $("#actorOptions").html(childs.join(" "));
    console.log("Success");
}

function actorError(error) {
    console.log(error);
}

function genreSuccess(response) {
    genres = response;
    let childs = [];
    genres.forEach(genre => {
        childs.push(`<option value="${genre.id}">${genre.name}</option>`)
    });
    $('#genreOptions').empty()
    $("#genreOptions").html(childs.join(" "));
    console.log("Success");
}

function genreError(error) {
    console.log(error);
}


function addMovie() {

    var formData = new FormData();
    var imageFile = $("#moviePoster")[0].files;
    formData.append('file', imageFile[0]);
    console.log(formData);
    $.ajax({
        type: 'POST',
        url: "https://localhost:5001/movies/upload",
        data: formData,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function (imageUrl) {

            console.log(imageUrl);
            let o = JSON.stringify(
                {
                    "Name": $("#movieName").val(),
                    "YearOfRelease": $("#yearOfRelease").val(),
                    "Plot": $("#moviePlot").val(),
                    "PosterURL": imageUrl,
                    "ProducerId": $("#producerSelect").val(),
                    "ActorIds": $("#actorOptions").val(),
                    "GenreIds": $("#genreOptions").val()
                });
            console.log(o);
            $.ajax({
                url: "https://localhost:5001/movies",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: o,
                success: function () {
                    window.alert("success");
                    location.href = "homepage.html";
                },
                error: function (error) {
                    window.alert("error");
                    console.log(error); 
                },
                async: false
            });
        }

})}


