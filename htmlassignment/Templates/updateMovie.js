$(document).ready(function () {
    $("#editGenres").select2();
    $('#editActors').select2();
    $.ajax({ url: "https://localhost:5001/producers", type: "GET", success: producerSuccess, error: producerError });
    $.ajax({ url: "https://localhost:5001/actors", type: "GET", success: actorSuccess, error: actorError });
    $.ajax({ url: "https://localhost:5001/genres", type: "GET", success: genreSuccess, error: genreError });
    let query = window.location.search;
    let urlParams = new URLSearchParams(query);
    let id = urlParams.get('id');
    $.ajax({ url: "https://localhost:5001/movies/" + id, type: "GET", success: movieSuccess, error: movieError });
});

var producers = [];
var actors = [];
var genres = [];

function producerSuccess(response) {
    producers = response;
    populateProducersList();
}

function producerError(error) {
    console.log(error);
}

function populateProducersList() {
    let childs = [`<option value="_">Producer</option>`];
    producers.forEach(producer => {
        childs.push(`<option value="${producer.id}">${producer.name}</option>`);
    });
    $("#editProducer").html(childs.join(''));
    console.log("success");
}

function actorSuccess(response) {
    actors = response;
    populateActorsList();
}

function actorError(error) {
    console.log(error);
}

function populateActorsList() {
    let childs = [];
    actors.forEach(actor => {
        childs.push(`<option value="${actor.id}">${actor.name}</option>`);
    });
    $("#editActors").html(childs.join(" "));
    console.log("Success");
}

function genreSuccess(response) {
    genres = response;
    populateGenresList();
}

function genreError(error) {
    console.log(error);
}

function populateGenresList() {
    let childs = [];
    genres.forEach(genre => {
        childs.push(`<option value="${genre.id}">${genre.name}</option>`)
    });
    $("#editGenres").html(childs.join(" "));
    console.log("Success");
}

function addActor() {
    let postData = JSON.stringify({
        "name": $("#actorName").val(),
        "bio": $("#actorBio").val(),
        "dob": $("#actorDob").val(),
        "sex": $("#actorSex").val()
    });
    $.ajax({
        url: "https://localhost:5001/actors",
        type: "POST",
        data: postData,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            $.ajax({
                url: "https://localhost:5001/actors/" + response.id,
                type: "GET",
                success: function (response) {
                    actors.push(response);
                    $("#addActorClose").trigger("click");
                    $("#addActorForm").trigger("reset");
                    populateActorsList();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        },
        error: function (error) {
            alert("Error");
            console.log(error);
        }
    });
    alert("Adding actor successfully completed.");
}


function addProducer() {
    let postData = JSON.stringify({
        "name": $("#producerName").val(),
        "bio": $("#producerBio").val(),
        "dob": $("#producerDob").val(),
        "sex": $("#producerSex").val()
    });
    $.ajax({
        url: "https://localhost:5001/producers",
        type: "POST",
        data: postData,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            $.ajax({
                url: "https://localhost:5001/producers/" + response.id,
                type: "GET",
                success: function (response) {
                    producers.push(response);
                    $("#addProducerClose").trigger("click");
                    $("#addProducerForm").trigger("reset");
                    populateProducersList();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        },
        error: function (error) {
            console.log(error);
        }
    });
    alert("Adding producer successfully completed.");
}

function addGenre() {
    let postData = JSON.stringify({
        "name": $("#genreName").val()
    });
    $.ajax({
        url: "https://localhost:5001/genres",
        type: "POST",
        data: postData,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            $.ajax({
                url: "https://localhost:5001/genres/" + response.id,
                type: "GET",
                success: function (response) {
                    genres.push(response);
                    $("#addGenreClose").trigger("click");
                    $("#addGenreForm").trigger("reset");
                    populateGenresList();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        },
        error: function (error) {
            console.log(error);
        }
    });
    alert("Adding genre succesfully completed.");
}

function movieSuccess(response) {
    $("#editMovieName").val(response.name);
    $("#editYearOfRelease").val(response.yearOfRelease);
    $("#editProducer").val(response.producer.id);
    response.actors.forEach(actor => {
        $(`select[id="editActors"] option[value="${actor.id}"]`).attr("selected", "selected");
        $("#editActors").trigger("change");
    });
    response.genres.forEach(genre => {
        $(`select[id="editGenres"] option[value="${genre.id}"]`).attr("selected", "selected");
        $("#editGenres").trigger("change");
    });
    $("#editPlot").val(response.plot);
    $("#editMovieSubmit").attr("onclick", `editMovie(${response.id})`);
}

function movieError(error) {
    console.log(error);
}

function editMovie(id) {
    var formData = new FormData();
    var imageFile = $("#editPoster")[0].files;
    formData.append('file', imageFile[0]);
    console.log(formData);
    $.ajax({
        type: 'POST',
        url: "https://localhost:5001/movies/upload",
        data: formData,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function (imageUrl) {
            let putData = JSON.stringify({
                "name": $("#editMovieName").val(),
                "yearOfRelease": $("#editYearOfRelease").val(),
                "producerId": $("#editProducer").val(),
                "actorIds": $("#editActors").val(),
                "genreIds": $("#editGenres").val(),
                "plot": $("#editPlot").val(),
                "posterURL": imageUrl,
            });
            $.ajax({
                url: "https://localhost:5001/movies/" + id,
                type: "PUT",
                data: putData,
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    alert("Movie updated successfully.");
                    location.href = "homepage.html";
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });
}