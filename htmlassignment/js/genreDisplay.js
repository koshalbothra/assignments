$.ajax({ url: "https://localhost:5001/genres", type: "GET", success: genreSuccessFunction, error: genreErrorFunction })

function genreSuccessFunction(genres) {
    $('#genreCards').empty();
    genres.forEach(genre => {
        let genreCardsHTML = `<div class="card col-sm-4">
        <div class="card-body col-sm-6">
            <h4>${genre.name}</h4>
        </div>
        <div class="actionButtons col-sm-5">
        <button class="btn deleteButton" onclick =deleteGenre(${genre.id}) id = "${genre.id}_delete"><span class="material-icons">delete</span></button>
        <button class="btn updateButton" onclick = editGenre(${genre.id}) id = "${genre.id}_update"><span class="material-icons">edit</span></button>
        </div>
    </div>`;
        $(".genreCards").append(genreCardsHTML);
    });
}

function genreErrorFunction(error) {
    console.log(error);
}

const deleteGenre = (id) => {
    console.log('DELETE');
    console.log(id);
    const url = "https://localhost:5001/genres/" + id
    $.ajax({
        url: url,
        type: "DELETE",
        success: function (data) {
            console.info(data);
            $.ajax({ url: "https://localhost:5001/genres", type: "GET", success: genreSuccessFunction, error: genreErrorFunction })
        },
        error: function (err) {
            console.log(err)
            window.alert(err.responseText)
        }
    });
}


const editGenre = (id) => {
    $.get("https://localhost:5001/genres/" + id, function (response) {
        console.log("Edit", response);
        let name = "";
        name = response.name;
        const genreFrom = `
        <form id="editGenreForm">
                            <div>
                            <input
                            type="text"
                            value=${response.id}
                            name="editGenreId"
                            disabled
                            hidden
                          />
                          Name :
                          <br>
                          <input
                            type="text"
                            class="form-control"
                            placeholder="${response.name}"
                            id='newGenreName'
                            name='newGenreName'
                            required
                          />
                          <br>
                            </div>
                            <div class="col-12">
                                <button type="button" class="btn btn-secondary" id="editGenreClose" data-dismiss="modal">Close</button>
                                <button type="submit" id="submit-movie" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </form>     
         `
        $("#editGenreModalBody").html(genreFrom);
        $("#editGenreModal").modal("show");
        $("#editGenreForm").submit((e) => {
            e.preventDefault();
            console.log("change", e.target.newGenreName.value);
            const request = {
                id: e.target.editGenreId.value,
                name: e.target.newGenreName.value,
            };
            var o = JSON.stringify(request);
            console.log(o);
            $.ajax({
                url: "https://localhost:5001/genres/" + id,
                type: "PUT",
                contentType: "application/json; charset=utf-8",
                data: o,
                success: function () {
                    window.alert("success");
                    $('#editGenreClose').trigger("click");
                },
                error: function (error) {
                    console.log(error);
                },
                async: false
            });

            $.ajax({ url: "https://localhost:5001/genres", type: "GET", success: genreSuccessFunction, error: genreErrorFunction })
        });
    });
};