$.ajax({ url: "https://localhost:5001/producers", type: "GET", success: producerSuccessFunction, error: producerErrorFunction })

var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

function producerSuccessFunction(producers) {
    $('#producerCards').empty();
    producers.forEach(producer => {
        let date = new Date(producer.dob);
        let dateOfBirth = months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
        let producerCardsHTML = `<div class="card col-sm-12">
        <div class="card-body col-sm-10">
            <h4>${producer.name}</h4>
            <h5>${dateOfBirth}</h5>
            <h5>${producer.sex}</h5>
            <p>${producer.bio}</p>
        </div>
        <div class="actionButtons col-sm-2">
        <button class="btn deleteButton" onclick = deleteProducer(${producer.id}) id = "${producer.id}_delete"><span class="material-icons">delete</span></button>
        <button class="btn updateButton" onclick = editProducer(${producer.id}) id = "${producer.id}_update"><span class="material-icons">edit</span></button>
        </div>
    </div>`;
        console.log("Success");
        $(".producerCards").append(producerCardsHTML);
    });
}
function producerErrorFunction(error) {
    console.log(error);
}

const deleteProducer = (id) => {
    console.log('DELETE');
    console.log(id);
    const url = "https://localhost:5001/producers/" + id
    $.ajax({
        url: url,
        type: "DELETE",
        success: function (data) {
            console.info(data);
            $.ajax({ url: "https://localhost:5001/producers", type: "GET", success: producerSuccessFunction, error: producerErrorFunction })
        },
        error: function (err) {
            console.log(err)
            window.alert(err.responseText)
        }
    });
}

const editProducer = (id) => {
    $.get("https://localhost:5001/producers/" + id, function (response) {
        console.log("EDIt", response);

        const ProducerForm = `
        <form id="editProducerForm">
        <div>
        <div class="form-group">
                Name : ${response.name}
                <input type="text" class="form-control" id="ProducerName" placeholder="${response.name}" name='newProducerName'>
            </div>
          DOB : 
          ${new Date(response.dob).toDateString()}
          <div>
          <input type="date" class="form-control" name='newProducerDob'>
        </div>
        <br/>
        Bio :
        <textarea class="form-control" placeholder="${response.bio}" name='newProducerBio' rows="3"></textarea>
        <br/>
      Gender : ${response.sex}
        <select class="form-control" id="newProducerGender" name="sex">
            <option>Male</option>
            <option>Female</option>
        </select>
        <br>
        </div>
        <div class="col-12">
            <button type="button" class="btn btn-secondary" id="editProducerClose"
                data-dismiss="modal">Close</button>
            <button type="submit" id="submit-movie" class="btn btn-primary">
                Update
            </button>
        </div>
    </form>
            
         `;
        $("#editProducerModalBody").html(ProducerForm);
        $("#editProducerModal").modal("show");
        $("#editProducerForm").submit((e) => {
            e.preventDefault();
            const bio = e.target.newProducerBio.value
            const dob = e.target.newProducerDob.value
            const sex = e.target.sex.value
            const name = e.target.newProducerName.value
            const request = {
                id: response.id,
                name: name === "" ? response.name : name !== response.name ? name : response.name,
                bio: bio === "" ? response.bio : bio !== response.bio ? bio : response.bio,
                sex: sex === "" ? response.sex : sex !== response.sex ? sex : response.sex,
                dob: dob === "" ? response.dob : dob !== response.dob ? dob : response.dob,

            };
            var o = JSON.stringify(request);
            console.log(o);
            $.ajax({
                url: "https://localhost:5001/producers/" + id,
                type: "PUT",
                contentType: "application/json; charset=utf-8",
                data: o,
                success: function () {
                    window.alert("success");
                    $('#editProducerClose').trigger("click");
                },
                error: function (error) {
                    console.log(error);
                },
                async: false
            });
            $.ajax({ url: "https://localhost:5001/Producers", type: "GET", success: producerSuccessFunction, error: producerErrorFunction })
        });
    });
};