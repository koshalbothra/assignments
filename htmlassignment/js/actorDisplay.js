$.ajax({ url: "https://localhost:5001/actors", type: "GET", success: actorSuccessFunction, error: actorErrorFunction })

var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

function actorSuccessFunction(actors) {
    $('#actorCards').empty();
    actors.forEach(actor => {
        let date = new Date(actor.dob);
        let dateOfBirth = months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
        let actorCardsHTML = `<div class="card col-sm-12" id="${actor.id}">
        <div class="card-body col-sm-10">
            <h4>${actor.name}</h4>
            <h5>${dateOfBirth}</h5>
            <h5>${actor.sex}</h5>
            <p>${actor.bio}</p>
        </div>
        <div class="actionButtons col-sm-2">
        <button class="btn deleteButton" onclick = deleteActor(${actor.id}) id = "${actor.id}_delete"><span class="material-icons">delete</span></button>
        <button class="btn updateButton" onclick = editActor(${actor.id}) id = "${actor.id}_update"><span class="material-icons">edit</span></button>
        </div>
    </div>`;
        console.log("Success");
        $(".actorCards").append(actorCardsHTML);
    });
}

function actorErrorFunction(error) {
    console.log(error);
}

const deleteActor = (id) => {
    console.log('DELETE');
    console.log(id);
    const url = "https://localhost:5001/actors/" + id
    $.ajax({
        url: url,
        type: "DELETE",
        success: function (data) {
            console.info(data);
            $.ajax({ url: "https://localhost:5001/actors", type: "GET", success: actorSuccessFunction, error: actorErrorFunction })
        },
        error: function (err) {
            console.log(err)
            window.alert(err.responseText)
        }
    });
}

const editActor = (id) => {
    $.get("https://localhost:5001/actors/" + id, function (response) {
        console.log("Edit", response);

        const actorForm = `
        <form id="editActorForm">

        <div>
        <div class="form-group">
            Name: ${response.name}
                <input type="text" class="form-control" id="actorName" placeholder="${response.name}" name='newActorName'>
            </div>
          DOB : 
          ${new Date(response.dob).toDateString()}
          <div>
          <input type="date" class="form-control" name='newActorDob'>
        </div>
        <br/>
        Bio : ${response.bio}
        <textarea class="form-control" name='newActorBio'   rows="3"  placeholder="${response.bio}"></textarea>
        <br/>
      Gender : ${response.sex}
        <select class="form-control" id="newActorGender" name="sex">
            <option>Male</option>
            <option>Female</option>
        </select>
        <br>
        </div>
        <div class="col-12">
            <button type="button" class="btn btn-secondary" id="editActorClose"
                data-dismiss="modal">Close</button>
            <button type="submit" id="submit-movie" class="btn btn-primary">
                Update
            </button>
        </div>
    </form>
            
         `;
        $("#editActorModalBody").html(actorForm);
        $("#editActorModal").modal("show");
        $("#editActorForm").submit((e) => {
            e.preventDefault();
            const bio = e.target.newActorBio.value
            const dob = e.target.newActorDob.value
            const sex = e.target.sex.value
            const name = e.target.newActorName.value
            const request = {
                id: response.id,
                name: name === "" ? response.name : name !== response.name ? name : response.name,
                bio: bio === "" ? response.bio : bio !== response.bio ? bio : response.bio,
                sex: sex === "" ? response.sex : sex !== response.sex ? sex : response.sex,
                dob: dob === "" ? response.dob : dob !== response.dob ? dob : response.dob,

            };
            var o = JSON.stringify(request);
            console.log(o);
            $.ajax({
                url: "https://localhost:5001/actors/" + id,
                type: "PUT",
                contentType: "application/json; charset=utf-8",
                data: o,
                success: function () {
                    window.alert("success");
                    $('#editActorClose').trigger("click");
                },
                error: function (error) {
                    console.log(error);
                },
                async: false
            });
            $.ajax({ url: "https://localhost:5001/actors", type: "GET", success: actorSuccessFunction, error: actorErrorFunction })
        });
    });
};