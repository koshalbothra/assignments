$(document).ready(function(){
    $("#actorDOB").attr("max",new Date().toISOString().split("T")[0]);
    $("#producerDOB").attr("max",new Date().toISOString().split("T")[0]);
});

$.ajax({ url: "https://localhost:5001/movies", type: "GET", success: movieSuccessFunction, error: movieErrorFunction })
function movieSuccessFunction(response) {
    $('#movieCards').empty();
    response.forEach(movie => {
        let movieGenres = [];
        movie.genres.forEach(genre => {
            movieGenres.push(genre.name);
        });
        let movieActors = [];
        movie.actors.forEach(actor => {
            movieActors.push(actor.name);
        });
        let movieCardsHTML = `<div class="card col-sm-12">
        <img class="card-img-top col-sm-3"
            src=${movie.posterURL}>
        <div class="card-body col-sm-7">
            <span class="movieName">
               ${movie.name}
            </span>
            <span class="yearOfRelease">
                ${movie.yearOfRelease}
            </span>
            <br>
            <span class="genres">
            ${movieGenres.join(", ")}
            </span>
            <br>
            <p>${movie.plot}</p>
            <span class="producer">Producer: ${movie.producer.name} </span>
            <br>
            <span class="actors">Cast: ${movieActors.join(", ")}</span>
        </div>
        <div class="actionButtons col-sm-2">
        <button class="btn deleteButton" id = "${movie.id}_delete" onclick = deleteMovie(${movie.id})><span class="material-icons">delete</span></button>
        <button class="btn updateButton" onclick="location.href='addOrEdit.html?id=${movie.id}'" id = "${movie.id}_update"><span class="material-icons">edit</span></button>
        </div>
    </div>`;
        $(".movieCards").append(movieCardsHTML);
    });
}
function movieErrorFunction(error) {
    console.log(error);
}



function addActor() {
    
        var o = JSON.stringify(
            {
                "Name": $("#actorName").val(),
                "Sex": $("#actorGender").val(),
                "DOB": $("#actorDOB").val(),
                "Bio": $("#actorBio").val()
            });
        console.log(o);
        $.ajax({
            url: "https://localhost:5001/actors",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: o,
            success: function () {
                window.alert("success");
                $('#addActorClose').trigger("click");
                $("#add-actor").trigger("reset");
                $.ajax({ url: "https://localhost:5001/actors", type: "GET", success: actorSuccess, error: actorError });
            },
            error: function (error) {
                console.log(error);
            },
            async: false
        });
        $.ajax({ url: "https://localhost:5001/actors", type: "GET", success: actorSuccessFunction, error: actorErrorFunction })
    
}

function addGenre() {

    var o = JSON.stringify(
        {
            "Name": $("#genreName").val(),
        });
    console.log(o);
    $.ajax({
        url: "https://localhost:5001/genres",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: o,
        success: function () {
            window.alert("success");
            $('#addGenreClose').trigger("click");
            $("#add-genre").trigger("reset");
            $.ajax({ url: "https://localhost:5001/genres", type: "GET", success: genreSuccess, error: genreError });

        },
        error: function (error) {
            console.log(error);
        },
        async: false
    });
    $.ajax({ url: "https://localhost:5001/genres", type: "GET", success: genreSuccessFunction, error: genreErrorFunction })




}

function addProducer() {
    var o = JSON.stringify(
        {
            "Name": $("#producerName").val(),
            "Sex": $("#producerGender").val(),
            "DOB": $("#producerDOB").val(),
            "Bio": $("#producerBio").val()
        });
    console.log(o);
    $.ajax({
        url: "https://localhost:5001/producers",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: o,
        success: function () {
            window.alert("success");
            $('#addProducerClose').trigger("click");
            $("#add-producer").trigger("reset");
            $.ajax({ url: "https://localhost:5001/producers", type: "GET", success: producerSuccess, error: producerError });
        },
        error: function (error) {
            console.log(error);
        },
        async: false
    });
    $.ajax({ url: "https://localhost:5001/producers", type: "GET", success: producerSuccessFunction, error: producerErrorFunction })

}

const deleteMovie = (id) => {
    console.log('DELETE');
    console.log(id);
    const url = "https://localhost:5001/movies/" + id
    $.ajax({
        url: url,
        type: "DELETE",
        success: function (data) {
            console.info(data);
            $.ajax({ url: "https://localhost:5001/movies", type: "GET", success: movieSuccessFunction, error: movieErrorFunction })
        },
        error: function (err) {
            console.log(err)
            window.alert(err.responseText)
        }
    });
}




