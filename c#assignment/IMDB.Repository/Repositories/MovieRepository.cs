﻿using IMDB.Domain;
using IMDB.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMDB.Repository.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        readonly private List<Movie> _movieList;

        /// <summary> To initialize the movieList </summary>
        public MovieRepository()
        {
            _movieList = new List<Movie>();
        }
        public void Add(Movie movie)
        {
            _movieList.Add(movie);
        }

        public void Delete(int index)
        {
            _movieList.RemoveAt(index);
        }

        public List<Movie> Get()
        {
            return _movieList.ToList();
        }

        public void Test()
        {
            // Find movies released in year 2000
            // Find actors with name containing "K" from the above list
            // Find producer who produced maximum movies

            var movieOutput = from s in _movieList where s.YearOfRelease == 2012 select s;
            
        }

    }
}
